# hugo-plugin

# Features

* Auto release via git submodule

# Docker Agent

If you have a Docker environment, you can use [hugo-jnlp-agent](https://github.com/LinuxSuRen/hugo-jnlp-agent) to build your Hugo site.
